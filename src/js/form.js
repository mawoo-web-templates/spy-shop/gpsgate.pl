(function($) {
    $(document).ready(function() {
        function scroll(event) {
            event.preventDefault();
            var id = event.target.hash;
            console.log($(id).offset().top);
            $('html, body').animate({
                scrollTop: $(id).offset().top - 10
            }, 500);
        }
        $('.scroll-btn').on('click', scroll);

        var actionURL;
        $('#gpstraceDemoUser').on('click', function() {
            if ($(this).is(':checked')) {
                $('#user').val('demo');
                $('#passwd').val('demo');
                actionURL = $(this).parents('form').attr('action');
                $(this).parents('form').attr('action', 'http://track2.gpstrace.pl/Login.aspx');
                $(this).parents('form').find('#select_srv').attr('disabled', 'disabled');
            } else {
                $('#user').val('');
                $('#passwd').val('');
                $(this).parents('form').attr('action', actionURL);
                $(this).parents('form').find('#select_srv').removeAttr('disabled');
            }
        });

        $('#select_srv').on('change', function() {
            var aLink = $('.login-form'),
                selVal = $(this).val(),
                staticLink = "http://s";
            staticLink2 = ".gpsgate.pl/Login.aspx";
            $(aLink).attr('action', staticLink + selVal + staticLink2);
        });
    });
}(jQuery));