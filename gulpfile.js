var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var tinypng = require('gulp-tinypng-compress');
var htmlmin = require('gulp-htmlmin');

gulp.task('sass', function () {
    return gulp.src('./src/sass/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            // nested, expanded, compact, compressed
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./build/css'));
});

gulp.task('js', function () {
    return gulp.src('./src/js/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('all.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./build/js'));
});

gulp.task('copy-img-tinypng', function () {
    return gulp.src('./src/img/**/*.{png,jpg,jpeg}')
        .pipe(tinypng({
            key: '6_2Zvi5VbHQSrJKPfShhwUz5CjDdD97l',
            sigFile: './src/img/.tinypng-sigs',
            log: true
        }))
        .pipe(gulp.dest('./build/img'));
});

gulp.task('copy-html', function () {
    return gulp.src('./src/**/*.html')
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(gulp.dest('./build'));
});

gulp.task('copy-files', function () {
    var copy = {
        files: ['./src/*.ico', './src/*.png', './src/.htaccess', './src/robots.txt']
    };
    return gulp.src(copy.files)
        .pipe(gulp.dest('./build'));
});

gulp.task('build', ['sass', 'js', 'copy-img-tinypng', 'copy-html', 'copy-files'], function () {
    gulp.watch('./src/sass/**/*.scss', ['sass']);
    gulp.watch('./src/js/**/*.js', ['js']);
    gulp.watch('./src/**/*.html', ['copy-html']);
});